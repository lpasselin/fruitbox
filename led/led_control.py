import serial
import serial.tools.list_ports
import time
import array

DEBUG = False

class Led(object):
    LED_COMMAND_ON = [b'A', b'B', b'C', b'D']
    LED_COMMAND_OFF = [b'a', b'b', b'c', b'd']
    NB_LED = 4

    def __init__(self):
        port = self.find_arduino_port()
        self.ser = serial.Serial(port, 9600)
        self.test_communication()
        self.data = array.array('B', [0] * self.NB_LED)
        self.update_arduino()

    def find_arduino_port(self):
        arduino_port = None
        ports = serial.tools.list_ports.comports()
        for port, desc, hwid, in sorted(ports):
            print("{}: {} [{}]".format(port, desc, hwid))
            if hwid.startswith('USB VID:PID=1A86:7523'):
                arduino_port = port
                break
        if arduino_port is None and not DEBUG:
            raise Error("Can't find arduino usb port")
        return arduino_port

    def __setitem__(self, i, val):
        self.data[i - 1] = val
        self.update_arduino()

    def __getitem__(self, i):
        return self.data[i - 1]

    def update_arduino(self):
        if DEBUG:
            return

        for i, d in enumerate(self.data):
            if d:
                self.ser.write(self.LED_COMMAND_ON[i])
            else:
                self.ser.write(self.LED_COMMAND_OFF[i])

    def test_communication(self):
        if DEBUG:
            return
        print("Testing Arduino communication. Should not take more than 5 seconds...")
        self.ser.readline()  # Wait for arduino reboot on new serial connection
        self.ser.flush()
        self.ser.write(b't')
        line = self.ser.readline()
        if line == b'ack\r\n':
            print("Arduino communication works")
        else:
            raise EnvironmentError("Arduino communication failure...")


if __name__ == "__main__":
    led = Led()
    led[1] = 1
    led[2] = 1

    print("Done")