void setup() {

  // put your setup code here, to run once:
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  digitalWrite(2, HIGH);
  digitalWrite(3, HIGH);
  digitalWrite(4, HIGH);
  digitalWrite(5, HIGH);
  Serial.begin(9600);
  Serial.println("LED Ready");
}

void parse_serial() {
  switch (Serial.read()) {
    // LED ON
    case 'A':
      digitalWrite(2, LOW);
      break;
    case 'B':
      digitalWrite(3, LOW);
      break;
    case 'C':
      digitalWrite(4, LOW);
      break;
    case 'D':
      digitalWrite(5, LOW);
      break;
    // OFF
    case 'a':
      digitalWrite(2, HIGH);
      break;
    case 'b':
      digitalWrite(3, HIGH);
      break;
    case 'c':
      digitalWrite(4, HIGH);
      break;
    case 'd':
      digitalWrite(5, HIGH);
      break;
    case 't':
      Serial.println("ack");
      break;
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available() > 0) {
    parse_serial();
  }
}
