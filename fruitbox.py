from tkinter import Tk, Canvas, Button, Checkbutton, Entry, BOTTOM, LEFT
from PIL import ImageTk, Image
from time import sleep
import os
from camera.camera import Camera
from led.led_control import Led

root = Tk()
c = Camera()
led = Led()

IMG_DOWNLOAD_DIR = 'img_dump/'
def capture_and_display_preview():
    img = Image.open(c.capture_preview())
    canvas.image = ImageTk.PhotoImage(img)
    canvas.create_image(0, 0, image=canvas.image, anchor='nw')
    return

def button_capture_all_lights():
    for i in range(1, 5):
        # led[i] = 0
        pass

    for i in range(1, 5):
        led[i] = 1
        sleep(0.2)
        # TODO get last img number in folder and add to id
        print("watchout you overwriting probably")
        id = 0
        c.capture(filename="./data/" + "id_{:05d}_light_{:01d}_img_%01n.jpg".format(id, i))
        led[i] = 0

def led_toggle(i):
    led[i] = not led[i]

canvas = Canvas(root)
canvas.pack(fill="both", expand=True)
capture_and_display_preview()


capture_all_lights = Button(root, text="Capture", command=button_capture_all_lights).pack()
#button_download_all_new = Button(root, text="Download and delete all new from SD", command=download_all_new)

checkbutton_led1 = Button(root, text="Led 1", command=lambda *args: led_toggle(1)).pack(side=BOTTOM)
checkbutton_led2 = Button(root, text="Led 2", command=lambda *args: led_toggle(2)).pack(side=BOTTOM)
checkbutton_led3 = Button(root, text="Led 3", command=lambda *args: led_toggle(3)).pack(side=BOTTOM)
checkbutton_led4 = Button(root, text="Led 4", command=lambda *args: led_toggle(4)).pack(side=BOTTOM)


root.mainloop()


