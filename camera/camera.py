import subprocess
import os
# import pyexiv2
import pickle
import time

# gphoto2 --get-config imageformat
DEFAULT_IMAGEFORMAT = "0"  #"sRAW" is about 1/4 of RAW.  # 0=Largest Finest JPEG

TARGET_SDCARD = 0  # 0=RAM 1=SD_Card
DEFAULT_ISO = 4  # 800 is best for 60D.  # 0=Auto 1=100 2=200 3=400 4=800 5=1600 6=3200 7=6400 8=12800 9=25600
DEFAULT_SHUTTERSPEED = "1/100"  # Maxime used 1/250
DEFAULT_APERTURE = "11"  # Maxime used 5.6  # 2.8, 3.5, 4, 4.5,  5.6, 6.7, 8, 9.5, 11, 13, 16, 19, 22, 27, 32
PREVIEW_APERTURE = "5.6"  # Helps to see focus
PREVIEW_SHUTTERSPEED = "1/100"

# TODO: set to appropriate whitebalance for our LED lights
DEFAULT_WHITEBALANCE = 1
#Choice: 0 Auto
#Choice: 1 Daylight
#Choice: 2 Shadow
#Choice: 3 Cloudy
#Choice: 4 Tungsten
#Choice: 5 Fluorescent
#Choice: 6 Flash
#Choice: 7 Manual
# Choice: 8 Color Temperature


FOCUSMODE = 3  # 0=one_shot 3=Manual


class Camera(object):
    def __init__(self, auto_exposure_bracketing=True):
        self.focus_index = None
        self.capture_preview_on = False
        self.aeb = auto_exposure_bracketing
        self._subprocess_run("gphoto2 --reset")
        self.mode = None
        self.set_mode_default()

    def set_mode_default(self):
        if self.mode == "Default":
            return
        print("setting default mode")
        config_list = list()
        config_list.append("gphoto2")
        config_list.append(" --set-config imageformat=\"{}\"".format(DEFAULT_IMAGEFORMAT))
        config_list.append(" --set-config iso={}".format(DEFAULT_ISO))
        config_list.append(" --set-config capturetarget={}".format(TARGET_SDCARD))
        config_list.append(" --set-config /main/capturesettings/shutterspeed=\"{}\"".format(DEFAULT_SHUTTERSPEED))
        config_list.append(" --set-config /main/capturesettings/aperture=\"{}\"".format(DEFAULT_APERTURE))
        config_list.append(" --set-config /main/imgsettings/whitebalance={}".format(DEFAULT_WHITEBALANCE))
        config_list.append(" --set-config /main/capturesettings/drivemode=1")

        if self.aeb:
            config_list.append(" --set-config /main/capturesettings/aeb=\"+/- 2\"")
            config_list.append(" --set-config /main/capturesettings/drivemode=1")  # Continuous shot mode (otherwise AEB does not work)
        else:
            config_list.append(" --set-config /main/capturesettings/aeb=\"0\"")
        config_list.append(" --wait-event=1s")

        self._subprocess_run("".join(config_list))
        self.mode = "Default"

    def set_mode_preview(self):
        # Set aperture to see focus area better
        if self.mode == "Preview":
            return
        self._subprocess_run("gphoto2 --set-config /main/capturesettings/aperture=\"{}\" --set-config /main/capturesettings/shutterspeed=\"{}\" --wait-event=10ms".format(PREVIEW_APERTURE, PREVIEW_SHUTTERSPEED))
        self.mode = "Preview"

    def download_and_delete_all(self):
        self._subprocess_run("mkdir -p img_dump")
        # TODO test with --new
        self._subprocess_run("cd img_dump && gphoto2 --get-all-files --skip-existing --recurse")
        self._subprocess_run("gphoto2 --delete-all-files --recurse")

    def capture(self, single=False, filename="img_%05n.jpg"):
        self.set_mode_default()

        if single or (self.aeb==False):
            self._subprocess_run("gphoto2 --capture-image-and-download")
        elif self.aeb:
            # Watch out for this force-overwrite! It's there so the camera doesn't bug when there are duplicate filenames
            self._subprocess_run("gphoto2 --force-overwrite --filename \"{}\" --set-config /main/actions/eosremoterelease=2 --wait-event-and-download=3f --set-config /main/actions/eosremoterelease=4".format(filename))
            # self._subprocess_run("gphoto2 --force-overwrite --filename \"{}\" --wait-event-and-download=".format(filename))

    def capture_preview(self, remove=False):
        self.set_mode_preview()
        filename = "capture_preview.jpg"

        self._subprocess_run("gphoto2 --capture-preview --force-overwrite")
        if remove:
            os.remove(filename)
        self.set_mode_default()
        return filename

    def focus(self, index=10):
        if self.focus_index is None:
            self.focus_reset()

        offset = self.focus_index
        if offset > 0:
            direction = "Far"
        else:
            direction = "Near"

        # TODO check if no difference between 1 and 10
        for i in range(offset/10):
            self.focus_offset(direction, num=10)
        for i in range(offset % 10):
            self.focus_offset(direction, num=1)

    def focus_offset(self, direction, num=10):
        if direction == "Near":
            offset = -num
        elif direction == "Far":
            offset = num
        else:
            print("error, use Near or Far as direction (caps)")

        self._subprocess_run("gphoto2 " + "".join([" --set-config manualfocusdrive=\"{} 1\"".format(direction) for i in range(num)]) + " --wait-event=20ms")

        if self.focus_index is None:
            print("Warning, we don't know camera.focus_index yet. Use reset")
        else:
            self.focus_index += offset

    def focus_reset(self):
        # resets focus to minimum distance by calling manual focusdrive multiple times
        for i in range(27):
            self.focus_offset("Near", num=100)
        self.focus_index = 0

    def _subprocess_run(self, cmd: str) -> None:
        result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE)
        print(result.stdout)
        return result

    def manual_delete(self, index_start, index_end):
        for i in range(index_start, index_end):
            self._subprocess_run("gphoto2 --delete-file=\"/store_00020001/DCIM/100CANON/IMG_{}.JPG\"".format(i))



if __name__ == "__main__":
    import cv2
    cam = Camera(0)
    while True:
        img = cam.capture()
        if img is not None:
            cv2.imshow('frame', img)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
